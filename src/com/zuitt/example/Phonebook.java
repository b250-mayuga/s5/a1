package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {

    public ArrayList<Contact> contacts = new ArrayList<Contact>();

    public Phonebook() {
        contacts = new ArrayList<Contact>();
    }


    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    public void setContacts(Contact contact) {
        contacts.add(contact);
    }
}
