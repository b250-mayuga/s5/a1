import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");

    Phonebook phonebook = new Phonebook();

    Contact contact1 = new Contact("John doe", "09123456789", "Manila");
        Contact contact2 = new Contact("Jane doe", "09123456789", "Quezon");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

    if(phonebook.getContacts().isEmpty()){
        System.out.println("empty phonebook has no contacts");
    } else{
        for(Contact c: phonebook.getContacts()){
            printInfo(c);
        }
    }

    }

    public static void printInfo(Contact contact) {
        System.out.println(contact.getName());
        System.out.println();

        if(contact.getContactNumber() != null) {
            System.out.println(contact.getName() + " has the following registered number: " + contact.getContactNumber());
        }
        else {
            System.out.println(contact.getName() + " has no registered number");
        }

        if(contact.getAddress() != null) {
            System.out.println(contact.getName() + " has the following registered address: " + contact.getAddress());
        }
        else {
            System.out.println(contact.getName() + " has no registered address");
        }
    }

}
